<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('_student', function (Blueprint $table) {
            $table->increments('id');
            $table->string('studentId');
            $table->string('passWord');
            $table->string('stuName');
            $table->string('classRoom');
            $table->string('Room');
            $table->string('Score')->default(100);
            $table->string('Status')->default("Stu");
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_student');
    }
}
