-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 23, 2018 at 05:00 AM
-- Server version: 10.3.9-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sm_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_08_16_004604_create__student_table', 1),
(4, '2018_08_16_043757_create_sessions_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('5Bxzmk2P5ga5gbrYbsfzaaOQhPMKtFryP4CgijLX', NULL, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiWWZxRlZpSUV6eUlYcTAyRDdKM2JJQUNybWVVQk1QZDJTZ2trbTJjNiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MTY6Imh0dHA6Ly9sb2NhbGhvc3QiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19', 1535023501),
('HAGBNRVmyuIXqz0yQlUd1VdsKh0rvHGGq55jeJVc', NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiNU81TlZXd3RiRUFjb3FNOVdIbklXNW4zaTE2cGl1eTkzeDdPeG9VSiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MTY6Imh0dHA6Ly9sb2NhbGhvc3QiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19', 1537533905),
('hMgZTSDlcvmpoAVFKCXTYAB6SH1gpSeMXmgqonAj', NULL, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiMTFTR0lXZnhQYnd3MmZzYnJXOGx5ZUg3MDRuZUo2Ykxac2h5V2I2QyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MTY6Imh0dHA6Ly9sb2NhbGhvc3QiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19', 1534773799),
('mqowqzuv1cPQp6pDQGzSJJWttGuCoCL6wZ78aIBX', 1, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 'YTo1OntzOjY6Il90b2tlbiI7czo0MDoiY2p0enc2eVByTHpvZ2RuUjFzMW40aFFEcXZOY3NrMDYzME5QbllFYyI7czozOiJ1cmwiO2E6MDp7fXM6OToiX3ByZXZpb3VzIjthOjE6e3M6MzoidXJsIjtzOjI4OiJodHRwOi8vbG9jYWxob3N0L3N5c3RlbS9ob21lIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo1MDoibG9naW5fd2ViXzU5YmEzNmFkZGMyYjJmOTQwMTU4MGYwMTRjN2Y1OGVhNGUzMDk4OWQiO2k6MTt9', 1535075085),
('mTY0uMtbA8wqiBTpsOZjrbiADsQZ9UYFQTzb2bWm', 1, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.92 Safari/537.36', 'YTo0OntzOjY6Il90b2tlbiI7czo0MDoiSXlFeklSbWVvUWlONGZOMDI3eHRlVXZkVEVFWWJMOWFVU2ljS3praSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzY6Imh0dHA6Ly9sb2NhbGhvc3Qvc3lzdGVtL2FkZF9zdHVkZW50cyI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fXM6NTA6ImxvZ2luX3dlYl81OWJhMzZhZGRjMmIyZjk0MDE1ODBmMDE0YzdmNThlYTRlMzA5ODlkIjtpOjE7fQ==', 1537321231),
('sN4wT8Fem8OS9sSwbZFZhPozPfNJ2OPW4YIQNEMW', NULL, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiQU5JdlI3UXhvU3Mxb3hrdFJXcThoTlV6d1JGUEF5MjB3cmx3ZFA2UiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjE6Imh0dHA6Ly9sb2NhbGhvc3Q6ODA4MCI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=', 1535165384),
('vkaI1tkHt5ozxWnPXC7s4wLOpUZQMfGehsFa689y', NULL, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiMDY1RnZCc1ptRGRLbE1pUFNMQ09qSVlaUVV3UHdFclBReTV2RUxMSSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MTY6Imh0dHA6Ly9sb2NhbGhvc3QiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19', 1534994705),
('z3phhmube3AxOJvx6KSozrpBd0Dpz4d4VU1Oj8ff', NULL, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoia2JMbGhQRXZaTEFEVU80Y1hHakNaNmVtQzNTR2ozc2NFMWhSME5iWSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjE6Imh0dHA6Ly9sb2NhbGhvc3Q6ODA4MCI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=', 1535358641),
('zdNuiXcrXhTSnqjTFp71T67fHh9MtbWUSrSLTPv4', NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiNnNYd255QlBJYmE5T29DRzZEWTlvREdxNXlQaVY0alY3bzBXQVFNUyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MTY6Imh0dHA6Ly9sb2NhbGhvc3QiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19', 1537548781),
('zT5qu4low2X61mgX5qgWnEdlFhpiE3Z4TtiHMq9u', NULL, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoicDk1dlVXenM1aDJFWHFxMjRraUJobThXaTBqOXZBV2JvcmdCSXF1VyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MTY6Imh0dHA6Ly9sb2NhbGhvc3QiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19', 1534672542);

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE `test` (
  `id` int(10) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `test`
--

INSERT INTO `test` (`id`, `timestamp`) VALUES
(1, '2018-08-26 15:01:17');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `user` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Client',
  `position` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `user`, `email`, `password`, `status`, `position`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Patchara Meerit', 'admin', 'contact@patchara.in.th', '$2y$10$moOST/zp9oMgFK7dsXR7luW5eYOSM7H0WT56v5jl1kPIM/Vmun1yu', 'Admin', 'ฝ่ายปกครอง', 'UDdeeai3UHKNWOY1QcNGlgRO2Dv0td5sG63RyaWf14wSwQOrDhVr5qzmY7X2', '2018-08-18 19:17:22', '2018-08-18 19:17:22');

-- --------------------------------------------------------

--
-- Table structure for table `_student`
--

CREATE TABLE `_student` (
  `id` int(10) UNSIGNED NOT NULL,
  `studentId` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `passWord` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `stuName` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `classRoom` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `Room` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `Score` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT '100',
  `Status` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Stu',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `_student`
--

INSERT INTO `_student` (`id`, `studentId`, `passWord`, `stuName`, `classRoom`, `Room`, `Score`, `Status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, '2985', '$2y$10$4kyUlhXh3rw.0EluK5BgEOTD/5L5tocj3n0h2DuHsO9/dsli5vRcu', 'ชื่อ 1 ', 'ม.1', '1', '100', 'Stu', NULL, NULL, NULL),
(2, '2986', '$2y$10$s2E25Ssi0bqcHREjLzDuredt/EoKTN3ZKvQEjT.eP2TDxtcWWOKd.', 'ชื่อ 2 ', 'ม.1', '2', '100', 'Stu', NULL, NULL, NULL),
(3, '2987', '$2y$10$4vSIQEwd4ZTcTexBBn8j4.s7Ysj027iBddRO6DnhjP4MDgY3hPN0W', 'ชื่อ 3', 'ม.1', '3', '100', 'Stu', NULL, NULL, NULL),
(4, '2988', '$2y$10$3xKfSACT/moKEfs0cxcg/.bZwU65YvU9jh/3nFN0Vemv1h2LsGdJa', 'ชื่อ 4', 'ม.2', '1', '100', 'Stu', NULL, NULL, NULL),
(5, '2989', '$2y$10$8woPu/EjssszslyuLc/dZ.smCM6ywpgUIwEHHqsWRrekNhKK2ywwS', 'ชื่อ 5', 'ม.2', '2', '100', 'Stu', NULL, NULL, NULL),
(6, '2990', '$2y$10$crKniSlZhMiMkFwgJyxgyuK4CiFDklYsJy/j1O5bQ00ZjiIIggGMq', 'ชื่อ 6', 'ม.2', '3', '100', 'Stu', NULL, NULL, NULL),
(7, '2991', '$2y$10$70Qf8zJRhklUjCs2qNSwo.zHOInZpVOpDHg53FCNCWNsdOMS5IGcS', 'ชื่อ 7', 'ม.3', '1', '100', 'Stu', NULL, NULL, NULL),
(8, '2992', '$2y$10$9dTx0To8wINd1UdCaLIJxO3bH2vNBNVmoNG2CcWt8Y8cU.mEhI1J.', 'ชื่อ 8', 'ม.3', '2', '100', 'Stu', NULL, NULL, NULL),
(9, '2993', '$2y$10$IraSJHWUNYzELpV8a31AcuSyBCFB.WkIhfUyCKCn.g/3j7HeQViz.', 'ชื่อ 9', 'ม.3', '3', '100', 'Stu', NULL, NULL, NULL),
(10, '2994', '$2y$10$o5D/DH2h.ZVW70yDdI6nsOq7a3mTvPVuEvYT0s7bpo7V.rTMvD/aG', 'ชื่อ 10', 'ม.4', '1', '100', 'Stu', NULL, NULL, NULL),
(11, '2995', '$2y$10$dqaO8DnDtXsYIvEX49jEluy2p6skvrYxCrThCtP8tpQSQZE2Ny4BW', 'ชื่อ 11', 'ม.4', '2', '100', 'Stu', NULL, NULL, NULL),
(12, '2996', '$2y$10$X4lMlIX0my6TEohQdNYtNuyF5i/lyqI5GqfXUec26TzRzHf8UYC9a', 'ชื่อ 12', 'ม.4', '3', '100', 'Stu', NULL, NULL, NULL),
(13, '2997', '$2y$10$izALMmqcMpFCV/QgxpvmRuSBNZHewuRvpC67tGVQRM4PXhhzPryx2', 'ชื่อ 13', 'ม.5', '1', '100', 'Stu', NULL, NULL, NULL),
(14, '2998', '$2y$10$kUM8etm1CJORdDVGk6w23eB03/3eKV9mrc3oX359/fG/ejpvc1sQ.', 'ชื่อ 14', 'ม.5', '2', '100', 'Stu', NULL, NULL, NULL),
(15, '2999', '$2y$10$pEd5Yj8klfCB0lP2Gn0KZeJumhcFnukY95lYtMuAUjwo3UESNRmre', 'ชื่อ 15', 'ม.6', '1', '100', 'Stu', NULL, NULL, NULL),
(16, '3000', '$2y$10$z504i/Ct3xWkUEFdJvNJI.wVEnNdZiHTkFTetizoH3ViZ2TXB/8fK', 'ชื่อ 16', 'ม.6', '3', '100', 'Stu', NULL, NULL, NULL),
(17, '2985', '$2y$10$Elgon2lxvj/Lr6Rwzvnc6OXGOba4HPCcrC8O8j0SO28bS2jwHdqxS', 'ชื่อ 1 ', 'ม.1', '1', '100', 'Stu', NULL, NULL, NULL),
(18, '2986', '$2y$10$m5leCvb6akmsLTVY2UbouONx35Tg3sdpLSJbgiCsvpiYIaNdzRd8m', 'ชื่อ 2 ', 'ม.1', '2', '100', 'Stu', NULL, NULL, NULL),
(19, '2987', '$2y$10$9InqJpYfp6rQ895UCEICvexP5MPjfRCBoknNsGhfYbfavYHAsU4K.', 'ชื่อ 3', 'ม.1', '3', '100', 'Stu', NULL, NULL, NULL),
(20, '2988', '$2y$10$J2y4Jt1OkjxqO/BrkUH16u2nt9FYHS4IpG06PLPgDmBTXI1UF3AfK', 'ชื่อ 4', 'ม.2', '1', '100', 'Stu', NULL, NULL, NULL),
(21, '2989', '$2y$10$6syBSFvQxs/ca3P8akMSkeRqClQxOWEfQc/b0kFZ8IUEBTuRXI9eG', 'ชื่อ 5', 'ม.2', '2', '100', 'Stu', NULL, NULL, NULL),
(22, '2990', '$2y$10$aloXC8u/zUslesZbhgbne.pmkO.Wpv0mN69gAC5s8y31bNY2gwGmi', 'ชื่อ 6', 'ม.2', '3', '100', 'Stu', NULL, NULL, NULL),
(23, '2991', '$2y$10$P2BCzpFsPVIQv0toMP2h1OXC4xvIOBxj31qF/zw/AAoWBMwzAKjKe', 'ชื่อ 7', 'ม.3', '1', '100', 'Stu', NULL, NULL, NULL),
(24, '2992', '$2y$10$hnKxaZ87fVjb11jhte.R.uO2PSlxeLvT4WGOAJZW/ZiHWgcuV047u', 'ชื่อ 8', 'ม.3', '2', '100', 'Stu', NULL, NULL, NULL),
(25, '2993', '$2y$10$02szExBgKr4oW0lom0AUqOvXBLQhScMaAEMXytNO/PsQbRzccD/JG', 'ชื่อ 9', 'ม.3', '3', '100', 'Stu', NULL, NULL, NULL),
(26, '2994', '$2y$10$dgQzcNcjp7zGT67w79/RF.JNYJUC.0qpo6s2/n6DH6dc2U5xo2Df6', 'ชื่อ 10', 'ม.4', '1', '100', 'Stu', NULL, NULL, NULL),
(27, '2995', '$2y$10$gCZ55hY90uGt.mHSAxwyn.Gs7HKw.SSMlY4JIG8a165AqteiERx3i', 'ชื่อ 11', 'ม.4', '2', '100', 'Stu', NULL, NULL, NULL),
(28, '2996', '$2y$10$1HIMP90arnNNcRJ2CFa77OTIMVLvy46s3Yjn5Jq59khqsHPNaWwhW', 'ชื่อ 12', 'ม.4', '3', '100', 'Stu', NULL, NULL, NULL),
(29, '2997', '$2y$10$LUcbWHRXDvynS9l15ylFNe5peUgTZzH1R9Ch0fTBkCy5Kt4Dtyw2G', 'ชื่อ 13', 'ม.5', '1', '100', 'Stu', NULL, NULL, NULL),
(30, '2998', '$2y$10$r/lh902dDU4v/dPNNUcT0uYUhFX8xWIhDCh6e2umMA8Anr81.aU6O', 'ชื่อ 14', 'ม.5', '2', '100', 'Stu', NULL, NULL, NULL),
(31, '2999', '$2y$10$vhwnsrynpa5Q0r1nN5/j/.Zg.3V2jsKfLfDVfCSissqPBRhrQ21iS', 'ชื่อ 15', 'ม.6', '1', '100', 'Stu', NULL, NULL, NULL),
(32, '3000', '$2y$10$lzJs8pxnMrsRLaEDUA2ht.kDJt2oV8npwSerEDwQYAAmjbYWNvJES', 'ชื่อ 16', 'ม.6', '3', '100', 'Stu', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `_student`
--
ALTER TABLE `_student`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `test`
--
ALTER TABLE `test`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `_student`
--
ALTER TABLE `_student`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
