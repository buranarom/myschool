<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Student;
use Session;
use Illuminate\Support\Facades\Hash;
use Excel;
class FileController extends Controller {
      public function index()
    {
        return view('pages.addStu');
    }
    public function import(Request $request){
        if($request->hasFile('file')){
            $path = $request->file('file')->getRealPath();
            $data = Excel::load($path,  function($reader) { $reader->toArray(); $reader->noHeading(); }, 'UTF-8' )->get();
            if($data->count()){
               
                foreach ($data as $datacsv) {
                    $stumain[] = [
                        'SchId' => $datacsv[0],
                        'SchName' => $datacsv[1],
                        'IdCard' => $datacsv[2],
                        'StuId' => $datacsv[5],
                        'Prefix' => $datacsv[7],
                        'FirstName' => $datacsv[8],
                        'LastName' => $datacsv[9],
                        'Class' => $datacsv[3],
                        'Room' => $datacsv[4]
                    ];
                    $studetail[] = [
                        'FName_EN' => $datacsv[10],
                        'LName_EN' => $datacsv[11],
                        'DateOfBirth' => $datacsv[12],
                        'Gender' => $datacsv[6],
                        'Age_Year' => $datacsv[13],
                        'Age_Mon' => $datacsv[14],
                        'Blood' => $datacsv[15],
                        'Nationalism' => $datacsv[16],
                        'Nationality' => $datacsv[17],
                        'Religion' => $datacsv[18],
                        'NumOfBro' => $datacsv[19],
                        'NumOfYoungBro' => $datacsv[20],
                        'NumOfSis' => $datacsv[21],
                        'NumOfYoungSis' => $datacsv[22],
                        'SonofMan' => $datacsv[23]
                    ];
                    $familydetail[] = [
                        'statusmarry' => $datacsv[24],
                        'idcardOfdad' => $datacsv[25],
                        'prefixDad' => $datacsv[26],
                        'nameDad' => $datacsv[27],
                        'lastnameDad' => $datacsv[28],
                        'incomeOfdad' => $datacsv[29],
                        'phoneDad' => $datacsv[30],
                        'idcardOfmom' => $datacsv[31],
                        'prefixMom' => $datacsv[32],
                        'nameMom' => $datacsv[33],
                        'lastnameMom' => $datacsv[34],
                        'incomeOfMom' => $datacsv[35],
                        'phoneMom' => $datacsv[36]
                    ];
                    $parentdetail[] = [
                        'implication' => $datacsv[37],
                        'IdCardParent' => $datacsv[38],
                        'prefix' => $datacsv[39],
                        'nameParent' => $datacsv[40],
                        'lastnameParent' => $datacsv[41],
                        'income' => $datacsv[42],
                        'phoneNum' => $datacsv[43],
                        'houseCode' => $datacsv[44],
                        'houseNum' => $datacsv[45],
                        'villageNo' => $datacsv[46],
                        'street' => $datacsv[47],
                        'Sub-district' => $datacsv[48],
                        'District' => $datacsv[49],
                        'Province' => $datacsv[50],
                        'postalCode' => $datacsv[51],
                        'phoneHouse' => $datacsv[52]
                    ];
                    $stuinfo[] = [
                        'curhousecode' => $datacsv[53],
                        'curhouseNum' => $datacsv[54],
                        'curvillageNo' => $datacsv[55],
                        'curstreet' => $datacsv[56],
                        'curSub-district' => $datacsv[57],
                        'curDistrict' => $datacsv[58],
                        'curProvince' => $datacsv[59],
                        'curpostalCode' => $datacsv[60],
                        'curphoneHouse' => $datacsv[61],
                        'weight' => $datacsv[62],
                        'height' => $datacsv[63],
                        'disadvantage' => $datacsv[64],
                        'accommodation' => $datacsv[65],
                        'lackofuniform' => $datacsv[66],
                        'lackofstationery' => $datacsv[67],
                        'shortageofclasses' => $datacsv[68],
                        'lackoflunch' => $datacsv[69],
                        'disability' => $datacsv[70],
                        'distance1' => $datacsv[71],
                        'distance2' => $datacsv[72],
                        'distance3' => $datacsv[73],
                        'boardingtime' => $datacsv[74],
                        'schooltrips' => $datacsv[75],
                        'studenttype' => $datacsv[76],
                        'notallocated' => $datacsv[77]
                    ];
                }
                if(!empty($stumain && $studetail && $familydetail && $parentdetail && $stuinfo)){
                  $insertData1 =  DB::table('stumain')->insert($stumain);
                  $insertData2 =  DB::table('studetail')->insert($studetail);
                  $insertData3 =  DB::table('familydetail')->insert($familydetail);
                  $insertData4 =  DB::table('parentdetail')->insert($parentdetail);
                  $insertData5 =  DB::table('stuinfo')->insert($stuinfo);
                   if ($insertData1 && $insertData2 && $insertData3 && $insertData4 && $insertData5) {
                            Session::flash('success', 'นำเข้าข้อมูลสำเร็จ');
                        }else {
                            Session::flash('error', 'Error inserting the data..');
                            return back();
                        }
                        
                   //  dd($arr)   ;
                }

            }
            return back();
        }
    }
    public function downloadExcelFile($type){
        $products = Product::get()->toArray();
        return \Excel::create('expertphp_demo', function($excel) use ($products) {
            $excel->sheet('sheet name', function($sheet) use ($products)
            {
                $sheet->fromArray($products);
            });
        })->download($type);
    }
}
