<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class Student extends Model
{
   public $fillable = ['studentId','passWord','stuName','classRoom','Room','Score'];
}