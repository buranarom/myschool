@extends('layouts.admin') @section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            นำเข้าข้อมูลนักเรียน - Import Students
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> หน้าเเรก</a></li>
            <li>นักเรียน</li>
            <li class="active">นำเข้าข้อมูลนักเรียน</a>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box col-md-12">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa  fa-user-plus"></i> นำเข้าข้อมูลนักเรียน</a>
                </h3>
            </div>
            <div class="box-body">
                @if ( Session::has('success') )
                <div  class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <strong>{{ Session::get('success') }}</strong>
                </div>
                @endif 
                @if ( Session::has('error') )
                <div class="alert alert-danger alert-dismissible" role="alert">
                   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <strong>{{ Session::get('error') }}</strong>
                </div>
                @endif 
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <div>
                        @foreach ($errors->all() as $error)
                        <p>{{ $error }}</p>
                        @endforeach
                    </div>
                </div>
                @endif

                <form action="{{ route('import') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="exampleInputFile">อัพโหลดไฟล์ .xlsx</label>
                        <input type="file" name="file" id="exampleInputFile" required>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-block btn-primary">
                    </div>
                </form>

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection
