<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Login System</title>


    <link href="{{ asset('asset/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('asset/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('asset/Ionicons/css/ionicons.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('asset/css/dist/css/AdminLTE.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('asset/plugins/iCheck/square/blue.css') }}" rel="stylesheet" type="text/css">

   <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

</head>

<body class="hold-transition login-page">
    @yield('content')

    <!-- Scripts -->
    <script src="{{ asset('asset/jquery/dist/jquery.min.js') }}" defer></script>
    <script src="{{ asset('asset/bootstrap/dist/js/bootstrap.min.js') }}" defer></script>
    <script src="{{ asset('asset/plugins/iCheck/icheck.min.js') }}" defer></script>

    <script>
       $(function () {
        $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' /* optional */
        });
      });
    </script>
</body>

</html>
