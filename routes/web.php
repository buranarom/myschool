<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@index');

Auth::routes();

Route::group(['middleware' => ['auth', 'admin']], function ()
  {
      Route::get('/system/home', 'AdminController@index')->name('home');
      Route::get('/system/add_students', 'AdminController@importxlsx')->name('addStu');
      Route::post('/system/import', 'FileController@import')->name('import');
  });
